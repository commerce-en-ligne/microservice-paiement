package com.mPaiement.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;
@Entity
@DiscriminatorValue("CC")
public class CreditcartPayment extends Payment {
    private int cardNumber;
    private String dateExpiration;

    public CreditcartPayment() {
        super();
    }

    public CreditcartPayment(double amount, String datePayment, int cardNumber, String dateExpiration) {
        super(amount, datePayment);
        this.cardNumber = cardNumber;
        this.dateExpiration = dateExpiration;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(int cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(String dateExpiration) {
        this.dateExpiration = dateExpiration;
    }
}
